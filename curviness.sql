

-- is it possible these divisions of track are meaningful? Are they maybe blocks?

-- http://boundlessgeo.com/2012/01/getting-curvey/
-- rail lines metadata: http://catalog.data.gov/harvest/object/321ca54a-4def-44c9-8ba7-5d17d8c314cd, https://maps3.arcgisonline.com/ArcGIS/rest/services/A-16/FRA_US_Railway_Network/MapServer/6
-- rail lines shp from http://www.rita.dot.gov/bts/sites/rita.dot.gov.bts/files/publications/national_transportation_atlas_database/2014/zip/rail_lines.zip, http://www.rita.dot.gov/bts/sites/rita.dot.gov.bts/files/publications/national_transportation_atlas_database/2014/polyline
-- MGT is million gross tons, is unit in categories on den11code. (categorie here: http://www.fgdl.org/metadata/metadata_archive/fgdl_html/rails_2010.htm)

-- more on sinuosity: https://en.wikipedia.org/wiki/Sinuosity
-------
-- shp2pgsql -s 4269 rail_lines.shp | psql -d rail_lines2
-- shp2pgsql -s 4269 rail_nodes.shp | psql -d rail_lines
-- shp2pgsql -s 4269 -W Latin1 cb_2013_us_county_500k/cb_2013_us_county_500k.shp | psql -d rail_lines

-- per segment curvature.
select stateab, fraarcid, ST_Length(geom) / ST_Distance(ST_StartPoint(ST_GeometryN(geom, 1)), ST_EndPoint(ST_GeometryN(geom, 1))) as ratio from rail_lines where stateab in ('NY', 'CT', 'RI', 'MA', 'PA', 'NJ', 'DE', 'MD', 'DC', 'VA', 'NC', 'WV', 'NH', 'VT', 'ME') and passngr like 'A%' and net = 'M' and ST_Distance(ST_StartPoint(ST_GeometryN(geom, 1)), ST_EndPoint(ST_GeometryN(geom, 1))) > 0 order by ratio desc;
alter table rail_lines add column sinuosity numeric;
update rail_lines set sinuosity = ST_Length(geom) / ST_Distance(ST_StartPoint(ST_GeometryN(geom, 1)), ST_EndPoint(ST_GeometryN(geom, 1))) where ST_Distance(ST_StartPoint(ST_GeometryN(geom, 1)), ST_EndPoint(ST_GeometryN(geom, 1))) > 0;


-- manually fixing segments that don't match: some of this is unnecessary, but not all of it.
update rail_lines set subdiv = 'METROPOLITAN' where gid in (46364, 46195, 47123, 49474, 46245, 46246, 154869, 46243, 46195, 46465, 46365, 46378, 46195, 46182, 46193, 46196, 80539, 46707, 104180);
update rail_lines set subdiv = 'MID-ATLANTIC' where gid in (103927, 137442, 148092 );
--turns out there are two Metropolitan divisions -- that's silly, Amtrak
update rail_lines set subdiv = 'METROPOLITAN-NEC' where subdiv = 'METROPOLITAN' and stateab in ('NY', 'NJ', 'PA');

-- these ones are strangely unlabeled
update rail_lines set subdiv = 'PHILLY-HARRISBURG-MAINLINE-JBM' where fraarcid in (236282, 236277, 159980,204776,204821,207719,207333,207335,207463,272633,272634,236366,236285,236324,236304,236305,236292,236300,236308,236309,236321,236361,236368,236370,236372,236373,236392,236393,236430,236415,236416,236424,236434,236435,236447,236457,236463,266913,252808,254946,264431,266938,266943,271141,272635,272636,272637,272638,273690,273691,274778,274779,274862,274863,274864);
update rail_lines set subdiv = 'EMPIRE-JBM' where fraarcid in (259498, 210996, 210997, 211003, 210964, 277974,277975, 210978, 102703,102700, 102711,150905,61342,167438,210967,210980,211004,278677,277976,277977,279569,279571);
update rail_lines set subdiv = 'NEW HAVEN LINE' where fraarcid in (278679,278680, 210995, 210999);

-- find all solitary nulls (surrounded by the same subdiv on both sides)
update rail_lines this
  set subdiv = nextarc.subdiv
  from rail_lines nextarc, rail_lines prevarc
  where ( (this.tofranode = nextarc.frfranode or this.tofranode = nextarc.tofranode) and
          (this.frfranode = prevarc.frfranode or this.frfranode = prevarc.tofranode)
    ) and
  this.subdiv is null and nextarc.subdiv is not null and prevarc.subdiv is not null and 
  nextarc.subdiv = prevarc.subdiv and 
  this.passngr like 'A%' and this.net = 'M' and
  nextarc.passngr like 'A%' and nextarc.net = 'M' and
  prevarc.passngr like 'A%' and prevarc.net = 'M';

-- to test miscreants:
-- select nextarc.fraarcid, nextarc.subdiv, prevarc.fraarcid, prevarc.subdiv from rail_lines this
-- left join rail_lines nextarc on this.frfranode = nextarc.tofranode
-- left join rail_lines prevarc on this.tofranode = prevarc.frfranode
-- where this.fraarcid = 264426 and 
-- nextarc.passngr like 'A%' and nextarc.net = 'M' and
-- prevarc.passngr like 'A%' and prevarc.net = 'M';

-- select nextarc.fraarcid, nextarc.subdiv, prevarc.fraarcid, prevarc.subdiv from rail_lines this
-- left join rail_lines nextarc on (this.tofranode = nextarc.frfranode or this.tofranode = nextarc.tofranode)
-- left join rail_lines prevarc on (this.tofranode = prevarc.frfranode or this.tofranode = prevarc.tofranode)
-- where this.fraarcid = 264426 and 
-- nextarc.passngr like 'A%' and nextarc.net = 'M' and
-- prevarc.passngr like 'A%' and prevarc.net = 'M';

-- select prevarc.fraarcid, prevarc.subdiv from rail_lines this
-- left join rail_lines prevarc on this.tofranode = prevarc.frfranode
-- where this.fraarcid = 264426 and
-- prevarc.passngr like 'A%' and prevarc.net = 'M';

-- select nextarc.fraarcid, nextarc.subdiv, prevarc.fraarcid, prevarc.subdiv from rail_lines this
-- left join rail_lines nextarc on this.frfranode = nextarc.tofranode
-- left join rail_lines prevarc on this.tofranode = prevarc.frfranode
-- where this.fraarcid = 264426 and 
-- nextarc.passngr like 'A%' and nextarc.net = 'M' and
-- prevarc.passngr like 'A%' and prevarc.net = 'M';


drop table if exists division_lines;
drop table if exists division_lines_segmented;

create table division_lines as 
select ST_Transform(ST_LineMerge(ST_Union(geom)), 2263) as geom, subdiv as name from rail_lines where passngr like 'A%' and net = 'M' group by subdiv;
-- https://lists.osgeo.org/pipermail/postgis-users/2008-July/020482.html
create table division_lines_segmented as
SELECT name,
   -- take a substring if the length is greater than 152.4 meters  
   -- otherwise take the remainder
   ST_LineSubstring(geom, 2640*n/length,
   CASE
     WHEN 2640*(n+2) < length THEN 2640*(n+2)/length
     ELSE 1
   END) as geom
FROM
   (SELECT name,
   -- reverse the vertex order so that the upstream end of each  
   -- linestring will be the remainder
   ST_LineMerge(ST_Reverse(geom)) AS geom,
   ST_Length(geom) As length
   FROM division_lines ts
   ) t
CROSS JOIN generate_series(0,10000) n
WHERE n*2640/length < 1;


alter table division_lines_segmented add column length numeric; 
update division_lines_segmented set length = ST_Length(geom);
alter table division_lines_segmented add column id serial; 
alter table division_lines_segmented add primary key(id);
alter table division_lines_segmented add column curviness numeric;
update division_lines_segmented set curviness = ST_Length(geom) / ST_Distance(ST_StartPoint(geom), ST_EndPoint(geom)) where ST_Distance(ST_StartPoint(geom), ST_EndPoint(geom)) > 0;



-- portions of metropolitan line, "pittsburgh line" line include double-tracked bits.



-- does each county's section resolve to a line?
-- good for testing miscreant divisions
select stateab, cntyfips, cntyname subdiv as name, ST_GeometryType(ST_LineMerge(ST_Union(geom))) as geom 
from (
  select geom, subdiv, cntyfips, stateab 
  from rail_lines where passngr like 'A%' and net = 'M'
  ) as k 
where subdiv = 'PITTSBURGH LINE' group by subdiv, cntyfips, stateab;

-- how is this all distributed?
 -- 1.00 | 15309
 -- 1.01 |  3478
 -- 1.02 |  1597
 -- 1.03 |   969
 -- 1.04 |   623
 -- 1.05 |   446
 -- 1.06 |   349
 -- 1.07 |   241
 -- 1.08 |   175
 -- 1.09 |   150
 -- 1.10 |   134
 -- 1.2  |   261
 -- 1.3  |    97
 -- 1.4  |    33
 -- 1.5  |    20
 -- 1.6  |     8
 -- 1.7  |     8
 -- 1.8  |     6
 -- 1.9  |     2
 -- 2.0  |     2
 -- 2.1  |     1
 -- 2.3  |     1
 -- 2.4  |     1
 -- 2.6  |     1
 -- 2.8  |     1
 -- 3.5  |     1
 -- 3.8  |     1
 -- 5.1  |     1
select substring((((curviness*100)::integer)::numeric/100)::text from 1 for 4) asdf, count(*) cnt from division_lines_segmented group by ((curviness*100)::integer) order by asdf desc; 

--NEC only: where subdiv in ('MID-ATLANTIC', 'METROPOLITAN-NEC', 'NEW HAVEN LINE', 'NEW ENGLAND', 'NEW YORK TERMINAL');

-- Questions:
-- Do the segments/nodes have any meaning? (Are they train signal blocks or something?)
-- Is there a meaning to the segments with null subdivision? 
-- Why two metropolitan subdivisions? (NJ/NY, a tiny bit of Bucks Cty. PA; DC-MD)

shp2pgsql -s 4269 amtrk_sta/amtrk_sta.shp | psql -d rail_lines


-- select the top 20 rows not within 2 miles (as the crow flies) from an Amtrak station.
(select name, id, curviness 
from division_lines_segmented 
where name in ('METROPOLITAN-NEC','MID-ATLANTIC','NEW HAVEN LINE', 'NEW YORK TERMINAL', 'NEW ENGLAND')
order by curviness desc limit 40
) EXCEPT 
(select name, id, curviness
from division_lines_segmented 
inner join amtrk_sta
on ST_Intersects(division_lines_segmented.geom, ST_Buffer(ST_Transform(amtrk_sta.geom, 2263), 10560))
where name in ('METROPOLITAN-NEC','MID-ATLANTIC','NEW HAVEN LINE', 'NEW YORK TERMINAL', 'NEW ENGLAND') and 
amtrk_sta.state in ('DC', 'MD', 'NJ', 'DE', 'PA', 'NY', 'CT', 'RI', 'MA')  
-- and amtrk_sta.stncode not in ('ABE', 'NRK', 'PHN', 'NBK', 'PJC', 'CWH',-- exclude stations skipped by some Northeast regionals
--                           'LGA', 
--                           'NRO', 'BRP', 'OSB', 'MYS', 'WLY', 'KIN') 
order by curviness desc limit 20)
order by curviness desc limit 20;

-- ANALYSIS

-- see curviest_lines_in_nec.txt for analysis of query below.
-- select name, id, curviness from division_lines_segmented where name in ('METROPOLITAN-NEC','MID-ATLANTIC','NEW HAVEN LINE', 'NEW YORK TERMINAL', 'NEW ENGLAND') order by curviness desc limit 20;


-- -- select the top 20 rows not within 5 miles (as the crow flies) from an Amtrak station.
-- (select name, id, curviness 
-- from division_lines_segmented 
-- where name in ('METROPOLITAN-NEC','MID-ATLANTIC','NEW HAVEN LINE', 'NEW YORK TERMINAL', 'NEW ENGLAND')
-- order by curviness desc limit 40
-- ) EXCEPT 
-- (select name, id, curviness
-- from division_lines_segmented 
-- inner join amtrk_sta
-- on ST_Intersects(division_lines_segmented.geom, ST_Buffer(ST_Transform(amtrk_sta.geom, 2263), 26400))
-- where name in ('METROPOLITAN-NEC','MID-ATLANTIC','NEW HAVEN LINE', 'NEW YORK TERMINAL', 'NEW ENGLAND') and 
-- amtrk_sta.state in ('DC', 'MD', 'NJ', 'DE', 'PA', 'NY', 'CT', 'RI', 'MA')  
-- -- and amtrk_sta.stncode not in ('ABE', 'NRK', 'PHN', 'NBK', 'PJC', 'CWH',-- exclude stations skipped by some Northeast regionals
-- --                           'LGA', 
-- --                           'NRO', 'BRP', 'OSB', 'MYS', 'WLY', 'KIN') 
-- order by curviness desc limit 20)
-- order by curviness desc limit 20;


-- state in ('DC', 'MD', 'NJ', 'DE', 'PA', 'NY', 'CT', 'RI', 'MA')  
-- and stncode not in ('ABE', 'NRK', 'PHN', 'NBK', 'PJC', 'CWH',-- exclude stations skipped by some Northeast regionals
--                           'LGA', 
--                           'NRO', 'BRP', 'OSB', 'MYS', 'WLY', 'KIN') 
-- 'YNY','ARD','POW','EXT','DOW','COT','PAR','RKV','CRT','WFD','MDN','BER','HFD','WND','WOB','FRA'


-- ogr2ogr -f "GeoJSON" curvy_nec_rails.geojson -t_srs EPSG:4326 PG:"dbname=rail_lines" "rail_lines"


-- stncode in ('RTE', 'BOS', 'BBY', 'PVD', 'NLC', 'NHV', 'STM', 'NYP', 'NWK', 'EWR', 'TRE', 'PHL', 'WIL', 'BAL', 'BWI', 'WAS', 'NCR',
-- 'ABE', 'NRK', 'PHN', 'NBK', 'PJC', 'CWH', 'NRO', 'BRP', 'OSB', 'MYS', 'WLY', 'KIN')



-- try to shingle every third
-- https://lists.osgeo.org/pipermail/postgis-users/2008-July/020482.html
create table division_lines_segmented_tenths as
SELECT name,
   -- take a substring if the length is greater than 152.4 meters  
   -- otherwise take the remainder
   ST_LineSubstring(geom, 526*n/length,
   CASE
     WHEN 526*(n+10) < length THEN 526*(n+10)/length
     ELSE 1
   END) as geom
FROM
   (SELECT name,
   -- reverse the vertex order so that the upstream end of each  
   -- linestring will be the remainder
   ST_LineMerge(ST_Reverse(geom)) AS geom,
   ST_Length(geom) As length
   FROM division_lines ts
   ) t
CROSS JOIN generate_series(0,10000) n
WHERE n*526/length < 1;
alter table division_lines_segmented_tenths add column length numeric; 
update division_lines_segmented_tenths set length = ST_Length(geom);
alter table division_lines_segmented_tenths add column id serial; 
alter table division_lines_segmented_tenths add primary key(id);
alter table division_lines_segmented_tenths add column curviness numeric;
update division_lines_segmented_tenths set curviness = ST_Length(geom) / ST_Distance(ST_StartPoint(geom), ST_EndPoint(geom)) where ST_Distance(ST_StartPoint(geom), ST_EndPoint(geom)) > 0;

-- this includes overlapping segments!
(select name, id, curviness 
from division_lines_segmented_tenths 
where name in ('METROPOLITAN-NEC','MID-ATLANTIC','NEW HAVEN LINE', 'NEW YORK TERMINAL', 'NEW ENGLAND')
order by curviness desc limit 300
) EXCEPT 
(select name, id, curviness
from division_lines_segmented_tenths 
inner join amtrk_sta
on ST_Intersects(division_lines_segmented_tenths.geom, ST_Buffer(ST_Transform(amtrk_sta.geom, 2263), 10560))
where name in ('METROPOLITAN-NEC','MID-ATLANTIC','NEW HAVEN LINE', 'NEW YORK TERMINAL', 'NEW ENGLAND') and 
amtrk_sta.state in ('DC', 'MD', 'NJ', 'DE', 'PA', 'NY', 'CT', 'RI', 'MA')  
-- and amtrk_sta.stncode not in ('ABE', 'NRK', 'PHN', 'NBK', 'PJC', 'CWH',-- exclude stations skipped by some Northeast regionals
--                           'LGA', 
--                           'NRO', 'BRP', 'OSB', 'MYS', 'WLY', 'KIN') 
order by curviness desc limit 300)
order by curviness desc limit 60;


with top_ten as (
  (select name, id, curviness, geom 
from division_lines_segmented_tenths 
where name in ('METROPOLITAN-NEC','MID-ATLANTIC','NEW HAVEN LINE', 'NEW YORK TERMINAL', 'NEW ENGLAND')
order by curviness desc limit 300
) EXCEPT 
(select name, id, curviness, division_lines_segmented_tenths.geom
from division_lines_segmented_tenths 
inner join amtrk_sta
on ST_Intersects(division_lines_segmented_tenths.geom, ST_Buffer(ST_Transform(amtrk_sta.geom, 2263), 10560))
where name in ('METROPOLITAN-NEC','MID-ATLANTIC','NEW HAVEN LINE', 'NEW YORK TERMINAL', 'NEW ENGLAND') and 
amtrk_sta.state in ('DC', 'MD', 'NJ', 'DE', 'PA', 'NY', 'CT', 'RI', 'MA')  
-- and amtrk_sta.stncode not in ('ABE', 'NRK', 'PHN', 'NBK', 'PJC', 'CWH',-- exclude stations skipped by some Northeast regionals
--                           'LGA', 
--                           'NRO', 'BRP', 'OSB', 'MYS', 'WLY', 'KIN') 
order by curviness desc limit 300)
order by curviness desc limit 60
)
select 
 distinct(least(tt1.id, tt2.id) || ',' || greatest(tt1.id, tt2.id))
from top_ten tt1, top_ten tt2
where ST_Intersects(tt1.geom, tt2.geom) and tt1.id != tt2.id;
